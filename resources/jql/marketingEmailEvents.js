function main() {
	return Events({
		from_date: '${fromDate}',
		to_date: '${toDate}'
	})
	.filter(function(event) {
		return event.name.indexOf('Marketing Email') !== -1;
	})
	.sortAsc('time')
	.map(function(event) {
		var flatEvent = {};
		Object.assign(flatEvent, event.properties);
		formatPropsForAthena(flatEvent);
		flatEvent.event_name = event.name;
		flatEvent.mixpanel_id = event.distinct_id;
		flatEvent.time = formatTimeForAthena(event.time);
		flatEvent.categories = JSON.stringify(flatEvent.categories);
		return flatEvent;
	});
}

function formatPropsForAthena(event) {
	for (var prop in event) {
		if (event.hasOwnProperty(prop)) {
			var newProp = prop;
			newProp = newProp.replace(/ /g, '_');
			newProp = newProp.toLowerCase();
			if (newProp !== prop) {
				event[newProp] = event[prop];
				delete event[prop];
			}
		}
	}
}

function formatTimeForAthena(time) {
	return new Date(time).toISOString().replace('T', ' ').replace('Z', '');
}