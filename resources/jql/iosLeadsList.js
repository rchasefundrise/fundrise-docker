var emailCutoff = 365;
var pageViewedCutoff = 180;

var todayTimestamp = new Date('${emailDate}').getTime();

function main() {
	return join(Events({
			from_date: '2018-01-01',
			to_date:   '${today}'
		}),
		People())
		.filter(function(tuple) {
			var event = tuple.event;
			var user = tuple.user;
			
			if(!event) {
				return false;
			}
			
			if(user && user.properties['Investor Conversion (Placed)']) {
				return false;
			}
			
			return event.name === 'Page Viewed' || event.name.indexOf('Marketing Email') > -1 || event.name.indexOf('Lead Onboarding') > -1 || event.name.indexOf('Automated Email') > -1 || event.name === 'Added to Waiting List';
		})
		.groupByUser(function(state, tuples) {
			state = state || {};
			
			for(var i = 0; i < tuples.length; i++) {
				var event = tuples[i].event;
				var user = tuples[i].user;
				
				if(user && event.name === 'Page Viewed') {
					if(eventWithinCutoff(event, pageViewedCutoff)) {
						state.pageViewed = 1;
						if(event.properties.$device === 'iPhone') {
							state.ios = 1;
							state.iosLastTime = event.time;
						}
						continue;
					}
				}
				
				if(event.name === 'Marketing Email Opened' || event.name === 'Lead Onboarding Opened' || event.name === 'Automated Email Opened') {
					if(eventWithinCutoff(event, emailCutoff)) {
						state.opened = 1;
						// if(event.properties['Browser'] == 'Safari mobile') {
						if(event.properties['User Agent String'] && event.properties['User Agent String'].indexOf('iPhone') > -1) {
							state.ios = 1;
							state.iosLastTime = event.time;
						}
					}
				}
				
				if(user && user.properties['Acquisition Date'] && todayTimestamp - new Date(user.properties['Acquisition Date']).getTime() < 1000 * 60 * 60 * 24 * pageViewedCutoff) {
					state.recentLeadInclude = 1;
				}
			}
			
			return state;
		})
		.filter(function(item) {
			var state = item.value;
			return item.value.ios && (state.opened || state.pageViewed || state.recentLeadInclude)
				&& (new Date(item.value.iosLastTime) > new Date('2019-01-01') === ('${engagedLeads}' === 'true'))
				;
		})
		.map(function(item) {
			return item.key[0];
		})
}

function eventWithinCutoff(event, cutoff) {
	return todayTimestamp - event.time < 1000 * 60 * 60 * 24 * cutoff;
}