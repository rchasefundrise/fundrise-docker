function main() {
	return People({})
	.filter(function (user) {
		return user.properties.old_email_mixpanel_id;
	})
	.map(function (user) {
		var oldToNewMixpanelId = {};
		oldToNewMixpanelId.oldMixpanelId = user.properties.old_email_mixpanel_id;
		oldToNewMixpanelId.newMixpanelId = user.distinct_id;
		return oldToNewMixpanelId;
	});
}