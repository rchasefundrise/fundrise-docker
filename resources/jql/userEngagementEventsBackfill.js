function main() {
	return join(
		Events({
			from_date: '${fromDate}',
			to_date: '${toDate}',
			event_selectors: [{event: 'Page Viewed'}]
		}),
		People({
			user_selectors: [{selector: 'user["$email"] != undefined'}]
		}),
		{type: 'right'}
	)
	.groupByUser(function(state, tuples) {
		state = state || {};
		for(var i = 0; i < tuples.length; ++i) {
			state.user = tuples[i].user;
			var event = tuples[i].event;
			if (event !== undefined) {
				state.lastWebPageView = new Date(event.time);
				if(event.properties.$device === 'iPhone') {
					state.lastIosWebPageView = new Date(event.time);
				}
			}
		}
		return state;
	})
	.map(function(item) {
		var state = item.value;
		var user = state.user;
		var iterableUser = {};
		iterableUser.email = user.properties.$email;
		iterableUser.dataFields = {};
		if (state.lastWebPageView) {
			iterableUser.dataFields.lastWebPageView = formatDate(state.lastWebPageView);
		}
		if (state.lastIosWebPageView) {
			iterableUser.dataFields.lastIosWebPageView = formatDate(state.lastIosWebPageView);
		}
		if (user.properties.$last_login) {
			iterableUser.dataFields.lastLogin = formatDate(user.properties.$last_login);
		}
		if (user.properties['Last iOS App Login']) {
			iterableUser.dataFields.lastIosAppLogin = formatDate(user.properties['Last iOS App Login']);
			if (!iterableUser.dataFields.lastLogin || iterableUser.dataFields.lastIosAppLogin > iterableUser.dataFields.lastLogin) {
				iterableUser.dataFields.lastLogin = iterableUser.dataFields.lastIosAppLogin;
			}
		}
		return iterableUser;
	})
	.filter(function(iterableUser) {
		return iterableUser.dataFields.lastWebPageView || iterableUser.dataFields.lastIosWebPageView
			|| iterableUser.dataFields.lastLogin || iterableUser.dataFields.lastIosAppLogin;
	});
}

function formatDate(date) {
	return new Date(date).toISOString().replace('T', ' ').replace(/\....Z/, '');
}