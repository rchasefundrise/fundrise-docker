FROM mariadb:10.3
COPY scripts/fundrise.sql /docker-entrypoint-initdb.d/fundrise.sql
COPY scripts/permissions.sql /docker-entrypoint-initdb.d/permissions.sql
RUN chmod -R 775 /docker-entrypoint-initdb.d