FROM openjdk:8u141-jdk

# *** Intall Tomcat ****

ENV CATALINA_HOME /usr/local/tomcat
ENV TOMCAT_HOME $CATALINA_HOME
ENV PATH $CATALINA_HOME/bin:$PATH
RUN mkdir -p "$CATALINA_HOME"
WORKDIR $CATALINA_HOME

# let "Tomcat Native" live somewhere isolated
ENV TOMCAT_NATIVE_LIBDIR $CATALINA_HOME/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}$TOMCAT_NATIVE_LIBDIR

# runtime dependencies for Tomcat Native Libraries
# Tomcat Native 1.2+ requires a newer version of OpenSSL than debian:jessie has available
# > checking OpenSSL library version >= 1.0.2...
# > configure: error: Your version of OpenSSL is not compatible with this version of tcnative
# see http://tomcat.10.x6.nabble.com/VOTE-Release-Apache-Tomcat-8-0-32-tp5046007p5046024.html (and following discussion)
# and https://github.com/docker-library/tomcat/pull/31
ENV OPENSSL_VERSION 1.1.0f-3
RUN set -ex; \
	if ! grep -q stretch /etc/apt/sources.list; then \
# only add stretch if we're not already building from within stretch
		{ \
			echo 'deb http://deb.debian.org/debian stretch main'; \
		} > /etc/apt/sources.list.d/stretch.list; \
		{ \
# add a negative "Pin-Priority" so that we never ever get packages from stretch unless we explicitly request them
			echo 'Package: *'; \
			echo 'Pin: release n=stretch'; \
			echo 'Pin-Priority: -10'; \
			echo; \
# ... except OpenSSL, which is the reason we're here
			echo 'Package: openssl libssl*'; \
			echo "Pin: version $OPENSSL_VERSION"; \
			echo 'Pin-Priority: 990'; \
		} > /etc/apt/preferences.d/stretch-openssl; \
	fi
RUN apt-get update && apt-get install -y --no-install-recommends \
		libapr1 \
		openssl="$OPENSSL_VERSION" \
	&& rm -rf /var/lib/apt/lists/*

# see https://www.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/KEYS
# see also "update.sh" (https://github.com/docker-library/tomcat/blob/master/update.sh)
ENV GPG_KEYS 05AB33110949707C93A279E3D3EFE6B686867BA6 07E48665A34DCAFAE522E5E6266191C37C037D42 47309207D818FFD8DCD3F83F1931D684307A10A5 541FBE7D8F78B25E055DDEE13C370389288584E7 61B832AC2F1C5A90F0F9B00A1C506407564C17A3 713DA88BE50911535FE716F5208B0AB1D63011C7 79F7026C690BAA50B92CD8B66A3AD3F4F22C4FED 9BA44C2621385CB966EBA586F72C284D731FABEE A27677289986DB50844682F8ACB77FC2E86E29AC A9C5DF4D22E99998D9875A5110C01C5A2F6059E7 DCFD35E0BF8CA7344752DE8B6FB21E8933C60243 F3A04C595DB5B6A5F1ECA43E3B7BBB100D811BBE F7DA48BB64BCB84ECBA7EE6935CD23C10D498E23
RUN set -ex; \
	for key in $GPG_KEYS; do \
		gpg --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
	done

ENV TOMCAT_MAJOR 7
ENV TOMCAT_VERSION 7.0.79

# https://issues.apache.org/jira/browse/INFRA-8753?focusedCommentId=14735394#comment-14735394
ENV TOMCAT_TGZ_URL https://www.apache.org/dyn/closer.cgi?action=download&filename=tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
# not all the mirrors actually carry the .asc files :'(
ENV TOMCAT_ASC_URL https://www.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz.asc

# if the version is outdated, we have to pull from the archive :/
ENV TOMCAT_TGZ_FALLBACK_URL https://archive.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
ENV TOMCAT_ASC_FALLBACK_URL https://archive.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz.asc

RUN set -x \
	\
	&& { \
		wget -O tomcat.tar.gz "$TOMCAT_TGZ_URL" \
		|| wget -O tomcat.tar.gz "$TOMCAT_TGZ_FALLBACK_URL" \
	; } \
	&& { \
		wget -O tomcat.tar.gz.asc "$TOMCAT_ASC_URL" \
		|| wget -O tomcat.tar.gz.asc "$TOMCAT_ASC_FALLBACK_URL" \
	; } \
	&& gpg --batch --verify tomcat.tar.gz.asc tomcat.tar.gz \
	&& tar -xvf tomcat.tar.gz --strip-components=1 \
	&& rm bin/*.bat \
	&& rm tomcat.tar.gz* \
	\
	&& nativeBuildDir="$(mktemp -d)" \
	&& tar -xvf bin/tomcat-native.tar.gz -C "$nativeBuildDir" --strip-components=1 \
	&& nativeBuildDeps=" \
		dpkg-dev \
		gcc \
		libapr1-dev \
		libssl-dev \
		make \
		openjdk-${JAVA_VERSION%%[-~bu]*}-jdk=$JAVA_DEBIAN_VERSION \
	" \
	&& apt-get update && apt-get install -y --no-install-recommends $nativeBuildDeps && rm -rf /var/lib/apt/lists/* \
	&& ( \
		export CATALINA_HOME="$PWD" \
		&& cd "$nativeBuildDir/native" \
		&& gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
		&& ./configure \
			--build="$gnuArch" \
			--libdir="$TOMCAT_NATIVE_LIBDIR" \
			--prefix="$CATALINA_HOME" \
			--with-apr="$(which apr-1-config)" \
			--with-java-home="$(docker-java-home)" \
			--with-ssl=yes \
		&& make -j "$(nproc)" \
		&& make install \
	) \
	# && apt-get purge -y --auto-remove $nativeBuildDeps \
	&& rm -rf "$nativeBuildDir" \
	&& rm bin/tomcat-native.tar.gz \
# sh removes env vars it doesn't support (ones with periods)
# https://github.com/docker-library/tomcat/issues/77
	&& find ./bin/ -name '*.sh' -exec sed -ri 's|^#!/bin/sh$|#!/usr/bin/env bash|' '{}' +

# verify Tomcat Native is working properly
RUN set -e \
	&& nativeLines="$(catalina.sh configtest 2>&1)" \
	&& nativeLines="$(echo "$nativeLines" | grep 'Apache Tomcat Native')" \
	&& nativeLines="$(echo "$nativeLines" | sort -u)" \
	&& if ! echo "$nativeLines" | grep 'INFO: Loaded APR based Apache Tomcat Native library' >&2; then \
		echo >&2 "$nativeLines"; \
		exit 1; \
	fi

# ***** End Tomcat *****

# ***** Install Maven *****

ARG MAVEN_VERSION=3.6.3
ARG SHA=26ad91d751b3a9a53087aefa743f4e16a17741d3915b219cf74112bf87a438c5
ARG USER_HOME_DIR="/root"
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven

COPY maven/settings-docker.xml /usr/share/maven/ref/

VOLUME "$USER_HOME_DIR/.m2"

RUN export MAVEN_OPTS="-Xmx2048m"

# **** End Maven ******

# *** Intall Chrome ****

ENV CHROME_URL https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN apt-get -y update
RUN apt-get -y install libappindicator3-1 fonts-liberation libxss1 lsb-release libgconf-2-4 xdg-utils xvfb
RUN wget -O /tmp/google-chrome-stable_current_amd64.deb $CHROME_URL
RUN dpkg -i /tmp/google-chrome-stable_current_amd64.deb

ENV CHROME_BIN /usr/bin/google-chrome-stable

# *** End Chrome ****

# set time zone or date tests will fail
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install Ruby and Sass
RUN apt-get -y update
RUN apt-get -y install build-essential
RUN apt-get -y install ruby-full 
RUN apt-get -y install autoconf
RUN apt-get -y install autogen
RUN apt-get -y install libtool

RUN gem install sass

# Install memcached
RUN apt-get install -y memcached

# Install apache httpd
RUN apt-get install -y apache2

#install yarn and nodejs
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN apt-get update && apt-get install -y yarn

RUN yarn global add grunt
RUN yarn global add cypress@3.8.3

# Install PhantomJS
RUN yarn global add phantom --unsafe-perm
ENV PATH $PATH:/usr/local/share/.config/yarn/global/node_modules/phantom/node_modules/.bin/

# Install karma Chrome runner
RUN yarn global add karma-chrome-launcher

# Install casperjs
RUN yarn global add casperjs

# Install aws-cli
RUN apt-get -y install python-pip
RUN pip install awscli --upgrade --user
RUN echo "PATH=$PATH:~/.local/bin" >> ~/.bashrc

# **** Configurations ******

# fundrise config
COPY .fundrise /root/.fundrise/

# Tomcat config
COPY tomcat/catalina.sh ${CATALINA_HOME}/bin/
COPY tomcat/fundrise-admin-hibernate.properties ${CATALINA_HOME}/lib/
COPY tomcat/fundrise-avatar-hibernate.properties ${CATALINA_HOME}/lib/
COPY tomcat/fundrise-hibernate.properties ${CATALINA_HOME}/lib/
COPY tomcat/cybersource.properties ${CATALINA_HOME}/lib/
COPY tomcat/cybersource ${CATALINA_HOME}/lib/cybersource/

# script helper
COPY scripts/startCornice.sh /root/startCornice.sh

# devcertificates
COPY devcert /root/devcert

# apache2 configurations
COPY apache/ports.conf /etc/apache2/
COPY apache/tomcat.conf /etc/apache2/conf-enabled/

WORKDIR /etc/apache2/mods-enabled

RUN ln -s ../mods-available/proxy.load
RUN ln -s ../mods-available/proxy.conf
RUN ln -s ../mods-available/proxy_http
RUN ln -s ../mods-available/proxy_http.load
RUN ln -s ../mods-available/proxy_fcgi.load
RUN ln -s ../mods-available/proxy_scgi.load
RUN ln -s ../mods-available/proxy_wstunnel.load
RUN ln -s ../mods-available/proxy_ajp.load
RUN ln -s ../mods-available/proxy_balancer.load
RUN ln -s ../mods-available/proxy_balancer.conf
RUN ln -s ../mods-available/proxy_express.load
RUN ln -s ../mods-available/authz_groupfile.load
RUN ln -s ../mods-available/headers.load
RUN ln -s ../mods-available/proxy_connect.load
RUN ln -s ../mods-available/proxy_ftp.load
RUN ln -s ../mods-available/proxy_ftp.conf
RUN ln -s ../mods-available/slotmem_shm.load
RUN ln -s ../mods-available/ssl.load
RUN ln -s ../mods-available/ssl.conf
RUN ln -s ../mods-available/socache_shmcb.load

# Set default encoding to en_US.UTF-8
RUN echo "export LANG=en_US.UTF-8" >> ~/.bashrc
RUN echo "export LANGUAGE=en_US.UTF-8" >> ~/.bashrc

# Clearing the webapps folder in Tomcat
RUN rm -rf $TOMCAT_HOME/webapps/*

# tagging information
ARG GIT_COMMIT=unspecified
LABEL git_commit=$GIT_COMMIT
ENV DOCKER_IMAGE_GIT_COMMIT=${GIT_COMMIT}

# default FUNDRISE_SOURCES_DIR to localDebugRepo, this should be set by pipelines to the path for the fundrise sources
ENV FUNDRISE_SOURCES_DIR /localDebugRepo

# Install Java 11 for Sonarcloud
RUN curl -LfsSo /tmp/openjdk-11.tar.gz https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.11%2B9/OpenJDK11U-jre_x64_linux_hotspot_11.0.11_9.tar.gz; \
    echo "144f2c6bcf64faa32016f2474b6c01031be75d25325e9c3097aed6589bc5d548 */tmp/openjdk.tar.gz" | sha256sum -c -; \
    mkdir -p /opt/java/openjdk-11; \
    cd /opt/java/openjdk-11; \
    tar -xf /tmp/openjdk-11.tar.gz --strip-components=1; \
    rm -rf /tmp/openjdk-11.tar.gz;

COPY resources/ /root/resources/
COPY jars/fundrise-scripts-1.0-SNAPSHOT.jar /root/
COPY jars/fundrise-admin.war $TOMCAT_HOME/webapps/
ENV AWS_REGION us-east-1

#ENTRYPOINT ["/bin/bash", "/root/startCornice.sh"]