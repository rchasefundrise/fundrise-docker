#!/bin/bash

cd /root

java -cp conf:/root/resources/:/root/fundrise-scripts-1.0-SNAPSHOT.jar -Xmx2G com.fundrise.script.testdata.dynamodb.TestDynamoDBLoader

service apache2 start

$CATALINA_HOME/bin/catalina.sh run