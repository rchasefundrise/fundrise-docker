DELETE FROM authorities WHERE user_id = 1;

UPDATE users SET users.username = 'master@fundrise.com' WHERE id = 1;

INSERT INTO authorities (id, created, modified, version, authority, user_id)
SELECT NULL, now(), now(), 0, a.authority, 1
FROM authorities a WHERE authority LIKE '%ADMIN%';